# Exercises By hand

A physical quantity (like temperature/ pressure) is measured at different place/time. The following sequence of measurements were made for the quantity.

```
45.6, 34.6, 90.4, 23.5, 34.9, 45.6
```

Calculate the following statistics for the above measurements

1. Average
2. Mean
3. Standard deviation
4. Median
5. Mode
6. Variance

What do these `values` signify for the above measurement ?


##### Calculate the above set of statistics for 

1. 
```
1.0, 1.0, 1.0, 1.0, 1.0
```

What difference do you see ? 

2. 
```
1.0, 1.0, 1.0, 1.0, 1000 
```
What values are impacted by the big value at the end ?

3. 

```
1.0, -1.0, 1.0, -1.0, 1.0, -1.0
```
What Observations can be made about the presence of negative numbers ?
Which of the above values depend on the order of measurements ?


### Frequncy based average
Consider a sequence of measurements given by 
```
1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3 
```
This can also be written as 
```
1 -> 3
2 -> 7
3 -> 3
```
Give a measurement and it's number of occurences ( 1 occurred 3 times in the above example), calculate the mean of the above samples.

## Probability
1. A box has 3 balls of different colors (red, blue and green). Close your eyes and pick one at random from the box. What is the probablity of choosing the red ball ?
2. In a box  with 3 red balls (no other colors), if we do the above  experiment again, what is the probability of choosing red ball ?
3. What is the maximum probability of any event ?
4. What is the probability for getting Heads when we toss a coin ?
5. Alternative definition for average/mean - Given that events occur with probability, find the mean value of measurement. For example, on an unfair die which gives the following probability for results 

```
1 -> .3
2 -> .1
3 -> .1
4 -> .1
5 -> .1
6 -> .2
```

What is the mean value when rolling the dice ? What does the mean value mean here? Does it say anything about a single roll of dice ? or about multiple roll of dice ? Here if we replace a dice by some measurement that has randomness, we can use the same principle to mean value of the measurement. How does this reflect in `real scenarios` ?
5. What does sampling mean ? How does it help in dealing with large datasets ?
6. What is the difference between uniformly random and other distribution ?
# Python
1. What are the datatypes in python ? Why does datatype matter (In terms of what you can do with it)? Can a string be added to an integer ? What datatypes can be used for math calculations ?
```
a = 1
b = 1.0
c = True
d="abc"
```
List the datatypes for each.

2. What are the mathematical operators in python ? What datatypes can they work on? What are the datatypes of the result of the operation ? What will be the datatype result of mathematical operations between `float` and `int` ?

3. What are logical operators ? WHat is the result of a logical operation ?

4. What is an error in python ? Divide 1 by 0 and observe the error. Add a string to integer. Observe the error. Do you see an error description ? 

5. What is a `list` in python ? Give two different ways of defining a variable a that is an empty list.

6. Can list contain elements of different datatypes ? Show with example. Create a `list` (A) of floating point numbers containing 10 elements. Use this for exercise 6.

7. 
		1. Use  `print` statement in python script to print "hello" on terminal
		2. Use `print` on a `list`. (This will print with `[]`)
		3. Now use `for` loop to print each element of a list.
		4. Use `for` loop to find  the sum of elements of a list. Print the sum.  Do you need an extra variable to find the sum ? 
		5. Now modify the same loop to calculate the varince and std deviation.
		6. Convert the above list into `numpy array`. What is a `numpy array` ? How is it different from python list ? Why is it preferred ?
8. Once you have numpy array, calculate the above stats using numpy functions. Numpy library is good because of this. It has many utilities. 

9. Create two numpy arrays of different length and inspect the result (type/error/ print values) for the following:
				* Use math operators on them (+, -, /, *)
				* Use boolean operators on them
10. Do the same operation for numpy arrays of same length. What result do you see ? can you interpret ?
11. List is one dimensional (There is only one dimension `A[1], A[5] ..` etc  . `A[1]` can means measurenment on first day). How about two dimensional data ? (`A[1][2]` = Measurement at first day on second location. So it  is a table). Try to draw list and table on paper and understand indexing on multidimensional arrays in python/numpy. Numpy can efficiently support higher dimensions. How will you visualise 3 dimensional data on paper ?  

When you do exercises, D not refer anything other than numpy references. It should be the only thing that you refer. Do not search on google unless you have spent five minutes on the question. 

12. Indexing is an important skill. It can be used very effectivley to data analysis. Numpy and pandas uses it very frequently. Now we have built  some understanding of numpy, do exercises [here][https://www.deep-teaching.org/notebooks/scientific-python/exercise-python-numpy-basics] and [here][https://www.deep-teaching.org/notebooks/scientific-python/exercise-python-numpy-basics]
13. DO exercises [here][https://pynative.com/python-numpy-exercise/]
14. install `seaborn` package on your python installation. Do not refer internet for installing - You should know how to install packages if you know the package Name.

## GEOMETRY


